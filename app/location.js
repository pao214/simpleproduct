var express = require('express');
var router = express.Router();
var monk = require('monk');
var db = monk('localhost:27017/vidzy');
var bodyparser = require('body-parser');
router.use(bodyparser.urlencoded({
	extended:true
}));
router.post('/',function(req,res){
	var collection = db.get('location');
	
	console.log(req.body);
	console.log(req.headers);
	collection.insert(
	{
		timestamp: req.body.timestamp,
		lat: req.body.lat,
		long: req.body.long
	},
	function(err,location){

		if(err) throw err;
		res.json({
			response:"success"
		});
	});
});
router.get('/', function(req, res) {
    var collection = db.get('location');
    collection.find({}, function(err, location){
        if (err) throw err;
      	res.json(location[location.length-1]);
    });
});
module.exports = router;