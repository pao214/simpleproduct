(function(){
	var app = angular.module('receiveRTC', [],
		function($locationProvider){$locationProvider.html5Mode(true);}
    );
	var client = new PeerManager();
	var mediaConfig = {
        audio:true,
        video: {
			mandatory: {},
			optional: []
        }
    };

    window.onunload = function(){
    	client.close();
    };

    // controller for remote streams
	app.controller('RemoteStreamsController', ['$location', '$http', function($location, $http){
		var rtc = this;
		rtc.remoteStreams = [];

		function getStreamById(id) {
		    for(var i=0; i<rtc.remoteStreams.length;i++) {
		    	if (rtc.remoteStreams[i].id === id) {return rtc.remoteStreams[i];}
		    }
		}

		rtc.loadData = function () {
			// get list of streams from the server
			$http.get('/streams.json').success(function(streams){
			    // get former state
			    for(var i=0; i<streams.length;i++) {
			    	var stream = getStreamById(streams[i].id);
			    	streams[i].isPlaying = (!!stream) ? stream.isPLaying : false;
			    }
			    // save new streams
			    rtc.remoteStreams = streams;
			});
		};

		rtc.view = function(stream){// add as a peer if not present already
			client.peerInit(stream.id, stream.route);
			stream.isPlaying = !stream.isPlaying;
		};
		var mobilelocation = function(){$http.get('/add_location').success(function(streams){
			console.log("hai");
			    mapmyindia_stock_marker(parseFloat(streams.lat),parseFloat(streams.long));
			});
	};

		//initial load
		rtc.loadData();
		console.log("hello");
		setInterval(mobilelocation,2000);
	}]);

















	var map = null;
            var marker = [];
            var visbility = false;
            var latitudeArr = [28.549948, 28.552232, 28.551748, 28.551738, 28.548602, 28.554603, 28.545639, 28.544339, 28.553196, 28.545842];
            var longitudeArr = [77.268241, 77.268941, 77.269022, 77.270164, 77.271546, 77.268305, 77.26480, 77.26424, 77.265407, 77.264195];
            window.onload = function() {
                var map_div = document.getElementById("map-container");
                var center = new mireo.wgs.point(28.54951289224, 77.267808616161);
                map = new mireo.map(map_div, {center: center, zoom: 4});
                map.on("multitap", function(e) {
                    var icon = mireo.stock_pins.narrow_pin_baloon_black();/*This object means for obtaining icons from MapmyIndia server. All stock icons have size 36px by 36px.*/
                    var title = "Text marker sample!";
                    marker.push(addMarker(e.wgs, icon, title));
                });
                map.on("long_press_begin", function(e) {
                    var icon = mireo.stock_pins.narrow_pin_baloon_green();/*This object means for obtaining icons from MapmyIndia server. All stock icons have size 36px by 36px.*/
                    var title = "Text marker sample!";
                    marker.push(addMarker(e.wgs, icon, title));/*e.wgs provide the wgs location*/ 
                });
            };
	function addMarker(position, icon, title, draggable) {
                var event_div = document.getElementById("event-log");
                var mk = new mireo.map.marker({
                    icon: icon, /*Each marker instance has to have icon/image associated with it (without it, it is simply not visible). We have specified url to
                     the image file on server disk, and also some offset within this image file (several "pins" are contained). */
                    handle_input: true, /*The handle_input flag must be set to true if the intended use of this object requires it to react to input events. */
                    draggable: draggable, /*The draggable flag is set to false, so this particular marker is not draggable on the map. */
                    title: title, /*The title sets the standard HTML5 title attribute, the position places this marker on the map. The z_order value specifies the relative z-order to the map.*/
                    position: position, /*must be instance of mireo.wgs.point that replaces current WGS position of this object. Will always return current WGS position.*/
                    map: map, /* The map parameter is crucial, because each map object adds itself to specified map, and if omitted, it would have been null, and consequently would not be seen on the map.*/
                    visible: true, /* If false, this object is not drawn in DOM. Default is true*/
                    z_order: 250/*z-order of this object when drawn on the map. The actual "z-index" CSS property used is calculated from this property, based on relative position to the current center 
                     point of the map in screen coordinates. The default value is 1.*/
                });
                /*The code that follows creation of the marker instance demostrates how to use the event handling via on function
                 * The following events:*/
                mk.on("select", function(e) {
                    event_div.innerHTML = "Marker select<br>"+event_div.innerHTML ;
                });
                mk.on("tap", function(e) {
                    event_div.innerHTML = "Marker tap<br>"+event_div.innerHTML ;
                });
                mk.on("multitap", function(e) {
                    event_div.innerHTML = "Marker multitap<br>"+event_div.innerHTML ;
                });
                mk.on("long_press_begin", function(e) {
                    event_div.innerHTML = "Marker long press begin<br>"+event_div.innerHTML ;
                });
                mk.on("long_press_end", function(e) {
                    event_div.innerHTML = "Marker long press end<br>"+event_div.innerHTML ;
                });
                return mk;
            }
            Array.max = function(array) {
                return Math.max.apply(Math, array);
            };
            Array.min = function(array) {
                return Math.min.apply(Math, array);
            };

            function mapmyindia_fit_markers_into_bound() {
                var sw = new mireo.wgs.point(Array.max(latitudeArr), Array.max(longitudeArr));/*south-west WGS location object*/
                var ne = new mireo.wgs.point(Array.min(latitudeArr), Array.min(longitudeArr));/*north-east WGS location object*/
                var bounds = new mireo.wgs.bounds(sw, ne);/*This class represents bounds on the Earth sphere, defined by south-west and north-east corners.i.e Creates a new WGS bounds.*/
                map.fit_bounds(bounds);/*Sets the center map position and level so that all markers is the area of the map that is displayed in the map area*/

            }
            function mapmyindia_removeMarker() {
                var markerlength = marker.length;
                if (markerlength > 0) {
                    for (var i = 0; i < markerlength; i++) {
                        marker[i].map(null);/* deletion of marker object */
                    }
                }
                delete marker;
                marker = [];
                document.getElementById("event-log").innerHTML = "";
                visbility = false;
            }
            function mapmyindia_stock_marker(lat,long) {
            	console.log(lat+" "+long);

                mapmyindia_removeMarker();
                var icon = mireo.stock_pins.pin_circle_red();/*This object provides means for obtaining icons from MapmyIndia server. All stock icons have size 36px by 36px.*/
                var postion = new mireo.wgs.point(lat, long);/*The WGS location object*/
                var title = "Stock Sample marker!";
                var mk = addMarker(postion, icon, title, false);
                marker.push(mk);
                map.set_center_and_zoom(mk.position(), 4);
            }
            function mapmyindia_custom_marker() {
                mapmyindia_removeMarker();
                var icon = new mireo.map.marker.image({
                    url: "d.png", /*local disk image url i.e URL to the actual image file*/
                    anchor: new mireo.base.point(0, -25), /*Relative negative position where top-left pixel of the (offset) image will be drawn, used to set top 
                     * and left CSS property of its own div element. If not specified, (0,0) is used.*/
                    size: new mireo.base.size(30, 40), /*Image size in pixels to be used, used to set width and height CSS property of its own div element. If not specified, (0,0) is used.*/
                    offset: new mireo.base.point(0, 0)/*Relative offset in pixels from image origin, passed to "margin-left" and "margin-top" CSS property of child img element. If not specified, (0,0) is used.*/
                });
                var postion = new mireo.wgs.point(28.5628, 77.6856);/*WGS location object*/
                var title = "Custom marker sample!";
                var mk = addMarker(postion, icon, title, false);
                marker.push(mk);
                map.center(mk.position());
            }
            function mapmyindia_multiple_markers() {
                mapmyindia_removeMarker();
                for (var i = 0; i < latitudeArr.length; i++) {
                    var icon = mireo.stock_pins.small_circle_red();/*This object means for obtaining icons from MapmyIndia server. All stock icons have size 36px by 36px.*/
                    var postion = new mireo.wgs.point(latitudeArr[i], longitudeArr[i]);/*WGS location object*/
                    var title = "Multiple markersample !";
                    marker.push(addMarker(postion, icon, title));
                }
                mapmyindia_fit_markers_into_bound();
            }
            function mapmyindia_number_on_marker() {
                mapmyindia_removeMarker();
                for (var i = 0; i < latitudeArr.length; i++) {
                    var icon = mireo.stock_pins.pin_circle_red();/*This object means for obtaining icons from MapmyIndia server. All stock icons have size 36px by 36px.*/
                    var postion = new mireo.wgs.point(latitudeArr[i], longitudeArr[i]);/*WGS location object*/
                    var title = "Number marker Sample!";
                    marker.push(addMarker(postion, icon, title));
                    marker[i].icon().text((i + 1), {/*css style used for number dispaly*/});/*The number to be displayed over image.*/

                }
                mapmyindia_fit_markers_into_bound();
            }
            function mapmyindia_text_on_marker() {
                mapmyindia_removeMarker();
                for (var i = 0; i < latitudeArr.length; i++) {
                    var icon = mireo.stock_pins.narrow_pin_baloon_red();/*This object means for obtaining icons from MapmyIndia server. All stock icons have size 36px by 36px.*/
                    var postion = new mireo.wgs.point(latitudeArr[i], longitudeArr[i]);/*WGS location object*/
                    var title = "Text marker sample!";
                    marker.push(addMarker(postion, icon, title));
                    marker[i].icon().text("MMI", {"font-size": "9px"/*css style used for text dispaly accordigly*/});/*The text to be displayed over image.*/

                }
                mapmyindia_fit_markers_into_bound();
            }
            function mapmyindia_draggable_marker() {
                mapmyindia_removeMarker();
                var icon = mireo.stock_pins.pin_circle_red();
                var postion = new mireo.wgs.point(28.5628, 77.6856);
                var title = "Darggable marker sample!";
                var mk = addMarker(postion, icon, title, true);
                var event_div = document.getElementById("event-log");
                /* following events can be assigned handler (for every instance of draggable marker(s))*/
                mk.on("pan_start event", function(e) {
                    event_div.innerHTML = "Marker pan_start<br>"+event_div.innerHTML ;
                });
                mk.on("pan_move event", function(e) {
                    event_div.innerHTML = "Marker pan_move event<br>"+event_div.innerHTML ;
                });
                mk.on("pan_end event", function(e) {
                    event_div.innerHTML = "Marker pan_end event<br>"+event_div.innerHTML ;
                });
                /*A special handler, "changed" is emitted at the end of the drag event (without parameters), that can be particularly useful. */
                mk.on("changed", function(e) {
                    var pt = mk.position();
                    event_div.innerHTML = "Draggable:</br> lat:" + pt.lat + "</br>lng:" + pt.lng + "</br>";
                });
                marker.push(mk);
                map.center(mk.position());/*get the wgs locaton from marker and set the locaton into center*/

            }
            function  mapmyindia_set_unvisible_marker() {
                var markerlength = marker.length;
                if (markerlength > 0) {
                    var visible = document.getElementById("visible");
                    visible.innerHTML = visbility ? "Hide Marker(s)" : "Show Marker(s)";
                    for (var i = 0; i < markerlength; i++) {
                        marker[i].visible(visbility);/* If false, this marker is not drawn in DOM. if true marker is drawn in DOM */
                    }
                }
                visbility = visbility ? false : true;
            }
       

})();
