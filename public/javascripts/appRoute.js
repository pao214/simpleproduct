(function(){
	var app = angular.module('routeRTC', [],
		function($locationProvider){$locationProvider.html5Mode(true);}
    );
	var client = new PeerManager();
	var mediaConfig = {
        audio:true,
        video: {
			mandatory: {},
			optional: []
        }
    };

    window.onunload = function(){
    	client.close();
    };

    // controller for remote streams
	app.controller('RemoteStreamsController', ['$location', '$http', function($location, $http){
		var rtc = this;
		
		rtc.remoteStreams = [];
		
		rtc.loadData = function () {
			// get list of streams from the server
			$http.get('/streams.json').success(function(streams){
			    // get former state
			    for(var stream in streams) {
			    	stream.isPlaying = true; // (!!stream) ? stream.isPLaying : false;
			    	client.peerInit(stream.id);
			    }

			    rtc.remoteStreams = streams;

			});
		};

		//initial load
		rtc.loadData();
	}]);
})();